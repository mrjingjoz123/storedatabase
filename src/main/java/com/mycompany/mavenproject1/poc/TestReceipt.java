/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1.poc;

import model.Customer;
import model.Product;
import model.Receipt;
import model.User;

/**
 *
 * @author 59161111
 */
public class TestReceipt {
    public static void main(String[] args) {
        Product p1 = new Product(1,"Americano",30);
        Product p2 = new Product(2,"Espresso",30);
        User seller = new User("Nadaed","111111111","password");
        Customer customer = new Customer("Somjai","9999999999");
        Receipt receipt = new Receipt(seller,customer);
        //go receipt.java
        receipt.addReceiptDetail(p1, 1);
        receipt.addReceiptDetail(p2, 3);
        System.out.println(receipt);
        receipt.addReceiptDetail(p1, 1);
//        receipt.deleteReciptDetail(0);
        System.out.println(receipt);
    }
    
}
